$(document).ready(function(){

    $('ul.tabs').delegate('li:not(.current)', 'click', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.section').find('div.box').hide().eq($(this).index()).fadeIn(150);
    });
  
    $('.modal-dialog .modal_forgot').hide();
    $('.modal-dialog .modal_forgot').find('.recovery_done').hide();

    $('.header_container .login a.registration').click(function(){
        $('.modal-dialog .modal_forgot').hide()
        $('.nav-tabs li.modal_enter').removeClass('active')
        $('.nav-tabs li.modal_registration').addClass('active')
        $('.modal-dialog .tab-content div.tab-pane.second').addClass('active in')
        $('.modal-dialog .tab-content div.tab-pane.first').removeClass('active in')
    })
    $('.header_container .login a.enter').click(function(){
        $('.modal-dialog .modal_forgot').hide()
        $('.nav-tabs li.modal_registration').removeClass('active')
        $('.nav-tabs li.modal_enter').addClass('active')
        $('.modal-dialog .tab-content div.tab-pane.second').removeClass('active in')
        $('.modal-dialog .tab-content div.tab-pane.first').addClass('active in')
    })
    $('.header_container .modal-dialog .forgot').click(function(){
        $('.modal-dialog .tab-content div.tab-pane').removeClass('active in')
        $('.nav-tabs li.modal_enter').removeClass('active')
        $('.modal-dialog .modal_forgot,.modal-dialog .modal_forgot form').fadeIn()
        $('.modal-dialog .modal_forgot').find('.done').hide()
    })
    $('.nav-tabs li.modal_registration,.nav-tabs li.modal_enter').click(function(){
        $('.modal-dialog .modal_forgot').hide()
    })
    $('.modal-dialog .modal_forgot').find('.recovery').click(function(){
        $('.modal-dialog .modal_forgot').find('form').hide()
        $('.modal-dialog .modal_forgot').find('.done').fadeIn();
    })
    $('.find').click(function(){
        $('.find').addClass('active')
       
    })
    if ($(window).width() < 1200) {
        var windowWidth = $(window).width()
        var imgPull = 1200 - windowWidth
        $('.main_page .devil_cloud img').css('left' , -imgPull/2)
        $('.main_page .angel_cloud img').css('right' , -imgPull/2)
    }
})
$(window).resize(function(){

    if ($(window).width() < 1200) {
        var windowWidth = $(window).width()
        var imgPull = 1200 - windowWidth
        $('.main_page .devil_cloud img').css('left' , -imgPull/2)
        $('.main_page .angel_cloud img').css('right' , -imgPull/2)
    }
})